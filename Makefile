ASM = nasm
ASMFLAGS = -f elf64
LD = ld

.PHONY: clean

all: lib.o main.o main

main.o: main.asm dict.asm words.inc colon.inc Makefile
	$(ASM) $)(ASMFLAGS) -g -F dwarf main.asm -o main.o

lib.o: lib.asm Makefile
	$(ASM) $)(ASMFLAGS) -g -F dwarf lib.asm -o lib.o

main: lib.o main.o Makefile
	$(LD) main.o lib.o -o main

clean:
	$(RM) *.o main
