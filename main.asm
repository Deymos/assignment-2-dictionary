%include "words.inc"
%include "lib.inc"
%include "dict.asm"

%define BUFF_SIZE 256

extern find_word
extern print_string
extern exit
extern print_newline

global _start

section .rodata
	not_found_msg db "String not found!", 0

section .text
	_start:
		sub rsp, BUFF_SIZE

		xor rax, rax
		xor rdi, rdi
		mov rsi, rsp
		mov rdx, 255
		syscall
		mov byte [rsp+rax], 0

		call print_newline

		mov rdi, rsp
		mov rsi, top_elem
		call find_word

		or rax, rax
		jz not_found
		lea rdi, [rax+8]
		mov rdi, qword [rdi]
		call print_string
		call print_newline
		xor rdi, rdi
		call exit

		not_found:
		mov rax, 1
		mov rdi, 2
		mov rsi, not_found_msg
		mov rdx, 17
		syscall

		call print_newline

		mov rdi, 1
		call exit

